# fullstack-testapp

## Requirements

* Node version 18.18.0
* Postman

## Installation

```
cd server
npm install
```

Use the provided Postman collection to send requests to the server.


## How did we get here

### Installing global packages

```
npm install -g typescript
```

### Installing server packages

```
cd server
npm install express
npm install -D tsx
npm install -D @types/node @types/express
npm install -D eslint
npm install -D nodemon tsx
npm install -D jest @types/jest
npm install -D supertest  @types/supertest

```

### Initializing server

```
tsc --init
npm init @eslint/config
```

## edit configs

* add to .eslintrc: `"jest": true`
* add to package.json 
  ```
  "scripts": {
    "test": "jest"
  },
  ```