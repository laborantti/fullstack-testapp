import { Request, Response } from "express";
import { Book } from "../models/book";

let books: Book[] = []; // This will act as a simple in-memory database

export const getAllBooks = (req: Request, res: Response) => {
	res.status(200).json(books);
};

export const getBook = (req: Request, res: Response) => {
	const book = books.find(b => b.id === parseInt(req.params.id));
	if (!book) return res.status(404).send("Book not found");
	res.status(200).json(book);
};

export const addBook = (req: Request, res: Response) => {
	const book: Book = req.body;
	books.push(book);
	res.status(201).send(book);
};

export const updateBook = (req: Request, res: Response) => {
	const book = books.find(b => b.id === parseInt(req.params.id));
	if (!book) return res.status(404).send("Book not found");
  
	book.title = req.body.title;
	book.author = req.body.author;
	res.status(200).send(book);
};

export const deleteBook = (req: Request, res: Response) => {
	const index = books.findIndex(b => b.id === parseInt(req.params.id));
	if (index < 0) return res.status(404).send("Book not found");

	books = books.filter(b => b.id !== parseInt(req.params.id));
	res.status(204).send();
};