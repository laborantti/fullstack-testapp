import { Router } from "express";
import { addBook, getAllBooks, getBook, updateBook, deleteBook } from "../controllers/bookController";

const router = Router();

router.get("/", getAllBooks);
router.get("/:id", getBook);
router.post("/", addBook);
router.put("/:id", updateBook);
router.delete("/:id", deleteBook);

export default router;