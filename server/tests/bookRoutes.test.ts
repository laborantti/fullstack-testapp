import request from "supertest";
import app from "../server";

describe("Books API", () => {
	it("GET /books - success", async () => {
		const result = await request(app).get("/books");
		expect(result.status).toEqual(200);
		expect(result.body).toBeInstanceOf(Array);
	});

	it("POST /books - success", async () => {
		const book = { id: 1, title: "New Book", author: "Author" };
		const result = await request(app).post("/books").send(book);
		expect(result.status).toEqual(201);
		expect(result.body).toHaveProperty("id", 1);
	});

	it("GET /books/:id - success", async () => {
		const book = { id: 1, title: "New Book", author: "Author" };
		await request(app).post("/books").send(book); // Setup step to ensure there is a book to fetch.
		
		const result = await request(app).get(`/books/${book.id}`);
		expect(result.status).toEqual(200);
		expect(result.body).toHaveProperty("id", book.id);
		expect(result.body).toHaveProperty("title", book.title);
		expect(result.body).toHaveProperty("author", book.author);
	});
	
	it("PUT /books/:id - success", async () => {
		const updatedBook = { title: "Updated Book Title", author: "Updated Author" };
		const bookId = 1; // Assuming this book ID exists
	
		const result = await request(app).put(`/books/${bookId}`).send(updatedBook);
		expect(result.status).toEqual(200);
		expect(result.body).toHaveProperty("title", updatedBook.title);
		expect(result.body).toHaveProperty("author", updatedBook.author);
	});
	
	it("DELETE /books/:id - success", async () => {
		const bookId = 1; // Assuming this book ID exists and is deletable
		const result = await request(app).delete(`/books/${bookId}`);
		expect(result.status).toEqual(204);
	
		// Optionally, verify the book is no longer present
		const getResult = await request(app).get(`/books/${bookId}`);
		expect(getResult.status).toEqual(404);
	});
});

